VERSION=`git describe | sed 's/^v//; s/-/./g' `
NAME="fontenay-sous-bois-themes"

prefix = /usr

all:
	@(echo "Nothing to build. Please use make install.")

install:
	mkdir -p $(DESTDIR)$(prefix)/share/authentic2/fontenay-sous-bois
	mkdir -p $(DESTDIR)$(prefix)/share/portail-citoyen2/fontenay-sous-bois
	mkdir -p $(DESTDIR)$(prefix)/share/wcs/themes/fontenay-sous-bois
	mkdir -p $(DESTDIR)$(prefix)/bin
	cp -r authentic2/* $(DESTDIR)$(prefix)/share/authentic2/fontenay-sous-bois/
	cp -r portail-citoyen/* $(DESTDIR)$(prefix)/share/portail-citoyen2/fontenay-sous-bois/
	cp -r wcs/fontenay-sous-bois/* $(DESTDIR)$(prefix)/share/wcs/themes/fontenay-sous-bois/
	cp -r common/static $(DESTDIR)$(prefix)/share/authentic2/fontenay-sous-bois/
	cp -r common/static $(DESTDIR)$(prefix)/share/portail-citoyen2/fontenay-sous-bois/
	cp -r common/static $(DESTDIR)$(prefix)/share/wcs/themes/fontenay-sous-bois/
	install -m 0755 wcs/update-fsb-template $(DESTDIR)$(prefix)/bin/

clean:
	rm -rf build sdist

build: clean
	mkdir -p build/$(NAME)-$(VERSION)
	for i in *; do \
		if [ "$$i" != "build" ]; then \
			cp -R "$$i" build/$(NAME)-$(VERSION); \
		fi; \
	done

dist-bzip2: build
	mkdir sdist
	cd build && tar cfj ../sdist/$(NAME)-$(VERSION).tar.bz2 .

version:
	@(echo $(VERSION))

name:
	@(echo $(NAME))

fullname:
	@(echo $(NAME)-$(VERSION))

